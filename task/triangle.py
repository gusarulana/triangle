def is_triangle(a, b, c):
    """
    Determine if a valid triangle with nonzero area can be constructed with the given side lengths.
    
    :param a: Length of the first side.
    :param b: Length of the second side.
    :param c: Length of the third side.
    
    :return: True if a triangle can be formed; False otherwise.
    """

    if not all(isinstance(x, (int, float)) and x >= 0 for x in [a, b, c]):
        return False

    return a + b > c and a + c > b and b + c > a
